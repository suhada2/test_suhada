﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CustomerService.Model;

namespace CustomerService.Controllers
{
    public class CustomerController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public readonly IConfiguration _configuration;
        private readonly SqlConnection _connection;
        public CustomerController(IConfiguration configuration)
        {
            _configuration = configuration;
            string connectionString = _configuration.GetConnectionString("Db_Conn");
            _connection = new SqlConnection(connectionString);
            _connection.Open(); // Open the connection once
        }

        // Ensure the connection is closed when the controller is disposed
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _connection.Close();
                _connection.Dispose();
            }
            base.Dispose(disposing);
        }

        [Route("api/Customer/getCustomer")]
        [HttpGet]
        public ActionResult<IEnumerable<ClsCustomer>> getCustomer()
        {
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT ID_PELANGGAN, NAMA, ALAMAT, NOMOR_TELEPON FROM [DB_TEST_MANDIRI].[dbo].[TBL_M_PELANGGAN]", _connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<ClsCustomer> CustomerList = new List<ClsCustomer>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ClsCustomer input = new ClsCustomer();
                    input.id_pelanggan = Convert.ToInt32(dt.Rows[i]["ID_PELANGGAN"]);
                    input.nama = Convert.ToString(dt.Rows[i]["NAMA"]);
                    input.alamat = Convert.ToString(dt.Rows[i]["ALAMAT"]);
                    input.nomor_telepon = Convert.ToString(dt.Rows[i]["NOMOR_TELEPON"]);
                    CustomerList.Add(input);
                }
            }

            if (CustomerList.Count > 0)
            {
                return StatusCode(StatusCodes.Status200OK, new { data = CustomerList });
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound, new { data = "Data Not Found" });

            }
        }

        [Route("api/Customer/getCustomerByID/{id}")]
        [HttpGet]
        public ActionResult<IEnumerable<ClsCustomer>> getCustomerByID(int id)
        {
            SqlDataAdapter da = new SqlDataAdapter($"SELECT ID_PELANGGAN, NAMA, ALAMAT, NOMOR_TELEPON FROM [DB_TEST_MANDIRI].[dbo].[TBL_M_PELANGGAN] WHERE ID_PELANGGAN = {id}", _connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<ClsCustomer> CustomerList = new List<ClsCustomer>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ClsCustomer input = new ClsCustomer();
                    input.id_pelanggan = Convert.ToInt32(dt.Rows[i]["ID_PELANGGAN"]);
                    input.nama = Convert.ToString(dt.Rows[i]["NAMA"]);
                    input.alamat = Convert.ToString(dt.Rows[i]["ALAMAT"]);
                    input.nomor_telepon = Convert.ToString(dt.Rows[i]["NOMOR_TELEPON"]);
                    CustomerList.Add(input);
                }
            }

            if (CustomerList.Count > 0)
            {
                return StatusCode(StatusCodes.Status200OK, new { data = CustomerList });
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { data = "Data Not Found" });

            }
        }

        [Route("api/Customer/CreateCustomer")]
        [HttpPost]
        public IActionResult CreateCustomer([FromBody] ClsCustomerInput param)
        {
            try
            {
                SqlCommand da = new SqlCommand($"INSERT INTO [dbo].[TBL_M_PELANGGAN] (NAMA, ALAMAT, NOMOR_TELEPON) VALUES ('{param.nama}','{param.alamat}','{param.nomor_telepon}')", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Disimpan !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Insert Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }

        [Route("api/Customer/UpdateCustomer")]
        [HttpPost]
        public IActionResult UpdateCustomer([FromBody] ClsCustomer param)
        {
            try
            {
                SqlCommand da = new SqlCommand($"UPDATE [dbo].[TBL_M_PELANGGAN] SET NAMA = '{param.nama}', ALAMAT = '{param.alamat}', NOMOR_TELEPON = '{param.nomor_telepon}'WHERE ID_PELANGGAN = {param.id_pelanggan}", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Diupdate !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Update Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }

        [Route("api/Customer/DeleteCustomer/{id}")]
        [HttpPost]
        public IActionResult DeleteCustomer(int id)
        {
            try
            {
                SqlCommand da = new SqlCommand($"DELETE FROM [dbo].[TBL_M_PELANGGAN] WHERE ID_PELANGGAN = {id}", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Dihapus !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Hapus Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }
    }
}
