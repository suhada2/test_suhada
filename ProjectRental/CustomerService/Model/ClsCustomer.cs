﻿namespace CustomerService.Model
{
    public class ClsCustomer
    {
        public int id_pelanggan { get; set; }
        public string nama { get; set; }
        public string alamat { get; set; }
        public string nomor_telepon { get; set; }
    }

    public class ClsCustomerInput
    {
        public string nama { get; set; }
        public string alamat { get; set; }
        public string nomor_telepon { get; set; }
    }
}
