using FluentValidation.Validators;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

var builder = WebApplication.CreateBuilder(args);

// Add configuration for Ocelot
builder.Configuration.SetBasePath(builder.Environment.ContentRootPath)
                      .AddJsonFile("ocelot.json", optional: false, reloadOnChange: true)
                      .AddEnvironmentVariables();

// Register CORS services
//builder.Services.AddCors(options =>
//{
//    options.AddPolicy("AllowSpecificOrigins", builder =>
//    {
//        builder.AllowAnyOrigin()
//                .AllowAnyHeader()
//                .AllowAnyMethod();
//    });
//});

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "allowCors", configurePolicy: policyBuilder =>
    {
        policyBuilder.WithOrigins("http://localhost:5202");
        //policyBuilder.AllowAnyOrigin();
        policyBuilder.AllowAnyHeader();
        policyBuilder.AllowAnyMethod();
        policyBuilder.AllowCredentials();
    });
});

// Register Ocelot services
builder.Services.AddOcelot(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

// Apply the CORS policy
//app.UseCors("AllowSpecificOrigins");

app.UseAuthorization();

// Map controllers if you have any
app.MapControllers();

// Use Ocelot middleware
await app.UseOcelot();

app.UseCors("allowCors");

app.Run();
