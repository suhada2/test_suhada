﻿using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class RentalController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Car()
        {
            return View();
        }

        public IActionResult Customer()
        {
            return View();
        }

        public IActionResult Ordering()
        {
            return View();
        }
    }
}
