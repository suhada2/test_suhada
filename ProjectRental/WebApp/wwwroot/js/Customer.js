﻿var table = $("#example1").DataTable({
    ajax: {
        type: "GET",
        url: "http://localhost:5205/Customer/getCustomer",
        dataSrc: "data",
    },
    "columnDefs": [
        { "className": "dt-center", "targets": '_all' },
        { "className": "dt-nowrap", "targets": '_all' }
    ],
    scrollX: true,
    "responsive": true, "lengthChange": false, "autoWidth": false,
    columns: [
        {
            "data": null,
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { data: 'nama' },
        { data: 'alamat' },
        { data: 'nomor_telepon' },
        {
            data: 'id_pelanggan',
            render: function (data, type, row) {
                action = `<div class="btn-group">`
                action += `<button type="button" class="btn btn-info me-1 mb-1" value="${row.id_pelanggan}" onclick="EditData(this)"><i class="fa fa-edit opacity-50 me-1"></i>Edit</button>`
                action += `<button type="button" class="btn btn-danger me-1 mb-1" value="${row.id_pelanggan}" onclick="DeleteData(this)"><i class="fa fa-trash opacity-50 me-1"></i>Delete</button>`
                action += `</div>`

                return action;
            }
        }
    ]
});

$("#btnAddData").on("click", function () {
    $("#txtNama").val("");
    $("#txtAlamat").val("");
    $("#txtNoTelepon").val("");
    $("#btnUpdate").hide();
    $("#btnSave").show();

});

$("#btnSave").on("click", function () {
    var obj = {
        nama: $("#txtNama").val(),
        alamat: $("#txtAlamat").val(),
        nomor_telepon: $("#txtNoTelepon").val()
    }
    console.log(obj);
    $.ajax({
        url: "http://localhost:5246/api/Customer/CreateCustomer",
        data: JSON.stringify(obj),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
            alert(data.remark);
            table.ajax.reload();
            $("#modal-default").modal('hide');
        },
        error: function (xhr) {
            alert(xhr.Message);
        }
    });
});

function EditData(data) {
    $("#modal-default").modal("show");
    $("#btnUpdate").show();
    $("#btnSave").hide();

    $.ajax({
        url: "http://localhost:5205/Customer/getCustomerByID/" + data.value,
        type: "GET",
        cache: false,
        success: function (result) {
            console.log(result.data[0].nama_mobil);
            $("#txtNama").val(result.data[0].nama);
            $("#txtAlamat").val(result.data[0].alamat);
            $("#txtNoTelepon").val(result.data[0].nomor_telepon);
            $("#hdIdPelanggan").val(result.data[0].id_pelanggan);
        }
    });
}

$("#btnUpdate").on("click", function () {
    var obj = {
        id_pelanggan: $("#hdIdPelanggan").val(),
        nama: $("#txtNama").val(),
        alamat: $("#txtAlamat").val(),
        nomor_telepon: $("#txtNoTelepon").val()
    }
    console.log(obj);
    $.ajax({
        url: "http://localhost:5246/api/Customer/UpdateCustomer",
        data: JSON.stringify(obj),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
            alert(data.remark);
            table.ajax.reload();
            $("#modal-default").modal('hide');
        },
        error: function (xhr) {
            alert(xhr.Message);
        }
    });
});

function DeleteData(data) {
    var userConfirmed = confirm("Apakah Yakin Ingin Menghapus Data?");

    if (userConfirmed) {
        $.ajax({
            url: "http://localhost:5246/api/Customer/DeleteCustomer/" + data.value,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                alert(data.remark);
                table.ajax.reload();
            },
            error: function (xhr) {
                alert(xhr.Message);
            }
        });
    }

}