﻿
$(document).ready(function () {
    getNamaDropdown();
    getmobilDropdown();
});

function getNamaDropdown() {
    $.ajax({
        url: "http://localhost:5205/Customer/getCustomer",
        type: "GET",
        cache: false,
        success: function (result) {
            console.log(result.data);
            $("#txtNama").empty();
            text = '<option></option>';
            $.each(result.data, function (key, val) {
                text += '<option value="' + val.id_pelanggan + '" data-nama="' + val.nama + '">' + val.nama + '</option>';

            });
            $("#txtNama").append(text);
        }
    });
}

function getmobilDropdown() {
    $.ajax({
        url: "http://localhost:5205/Car/getCar",
        type: "GET",
        cache: false,
        success: function (result) {
            console.log(result.data);
            $("#txtMobil").empty();
            text = '<option></option>';
            $.each(result.data, function (key, val) {
                text += '<option value="' + val.id_mobil + '" data-nama="' + val.nama_mobil + '">' + val.nama_mobil + '</option>';

            });
            $("#txtMobil").append(text);
        }
    });
}

var table = $("#example1").DataTable({
    ajax: {
        type: "GET",
        url: "http://localhost:5205/Ordering/getOrdering",
        dataSrc: "data",
    },
    "columnDefs": [
        { "className": "dt-center", "targets": '_all' },
        { "className": "dt-nowrap", "targets": '_all' }
    ],
    scrollX: true,
    "responsive": true, "lengthChange": false, "autoWidth": false,
    columns: [
        {
            "data": null,
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { data: 'nama_pemesan' },
        { data: 'nama_mobil' },
        { data: 'tanggal_pemesanan' },
        { data: 'tanggal_pengembalian' },
        { data: 'total_harga' },
        { data: 'status' },
        {
            data: 'id_pemesanan',
            render: function (data, type, row) {
                action = `<div class="btn-group">`
                action += `<button type="button" class="btn btn-info me-1 mb-1" value="${row.id_pemesanan}" onclick="EditData(this)"><i class="fa fa-edit opacity-50 me-1"></i>Edit</button>`
                action += `<button type="button" class="btn btn-danger me-1 mb-1" value="${row.id_pemesanan}" onclick="DeleteData(this)"><i class="fa fa-trash opacity-50 me-1"></i>Delete</button>`
                action += `</div>`

                return action;
            }
        }
    ]
});

$("#btnAddData").on("click", function () {
    $("#txtNama").val("");
    $("#txtMobil").val("");
    $("#txtStartDate").val("");
    $("#txtEndDate").val("");
    $("#btnUpdate").hide();
    $("#btnSave").show();
    $("#divStatus").hide();

});

$("#btnSave").on("click", function () {
    var obj = {
        id_mobil: $("#txtMobil").val(),
        id_pelanggan: $("#txtNama").val(),
        tanggal_pemesanan: $("#txtStartDate").val(),
        tanggal_pengembalian: $("#txtEndDate").val()
    }
    console.log(obj);
    $.ajax({
        url: "http://localhost:5006/api/Ordering/CreateOrdering",
        data: JSON.stringify(obj),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
            alert(data.remark);
            table.ajax.reload();
            $("#modal-default").modal('hide');
        },
        error: function (xhr) {
            alert(xhr.Message);
        }
    });
});

function EditData(data) {
    $("#modal-default").modal("show");
    $("#btnUpdate").show();
    $("#btnSave").hide();
    $("#divStatus").show();

    $.ajax({
        url: "http://localhost:5205/Ordering/getOrderingByID/" + data.value,
        type: "GET",
        cache: false,
        success: function (result) {
            console.log(result.data[0].nama_mobil);
            $("#txtNama").val(result.data[0].id_pelanggan);
            $("#txtMobil").val(result.data[0].id_mobil);
            $("#txtStartDate").val(result.data[0].tanggal_pemesanan);
            $("#txtEndDate").val(result.data[0].tanggal_pengembalian);
            $("#txtStatus").val(result.data[0].status);
            $("#hdIdPemesanan").val(result.data[0].id_pemesanan);
        }
    });
}

$("#btnUpdate").on("click", function () {
    var status = 0;
    if ($("#txtStatus").val() === "CLOSE") {
        status = 1;
    }

    var obj = {
        id_pemesanan: $("#hdIdPemesanan").val(),
        id_mobil: $("#txtMobil").val(),
        id_pelanggan: $("#txtNama").val(),
        tanggal_pemesanan: $("#txtStartDate").val(),
        tanggal_pengembalian: $("#txtEndDate").val(),
        status: status
    }
    console.log(obj);
    $.ajax({
        url: "http://localhost:5006/api/Ordering/UpdateOrdering",
        data: JSON.stringify(obj),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
            alert(data.remark);
            table.ajax.reload();
            $("#modal-default").modal('hide');
        },
        error: function (xhr) {
            alert(xhr.Message);
        }
    });
});

function DeleteData(data) {
    var userConfirmed = confirm("Apakah Yakin Ingin Menghapus Data?");

    if (userConfirmed) {
        $.ajax({
            url: "http://localhost:5006/api/Ordering/DeleteOrdering/" + data.value,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                alert(data.remark);
                table.ajax.reload();
            },
            error: function (xhr) {
                alert(xhr.Message);
            }
        });
    }

}