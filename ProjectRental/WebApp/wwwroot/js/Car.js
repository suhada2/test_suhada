﻿
$(document).ready(function () {
    test();
});

//$(function () {
//    $("#example1").DataTable({
//        "responsive": true, "lengthChange": false, "autoWidth": false,
//    });

//});

var table = $("#example1").DataTable({
    ajax: {
        type: "GET",
        url: "http://localhost:5205/Car/getCar",
        dataSrc: "data",
    },
    "columnDefs": [
        { "className": "dt-center", "targets": '_all' },
        { "className": "dt-nowrap", "targets": '_all' }
    ],
    scrollX: true,
    "responsive": true, "lengthChange": false, "autoWidth": false,
    columns: [
        {
            "data": null,
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { data: 'nama_mobil' },
        { data: 'warna' },
        { data: 'no_plat' },
        { data: 'harga' },
        //{ data: 'id_mobil' },
        {
            data: 'id_mobil',
            render: function (data, type, row) {
                action = `<div class="btn-group">`
                action += `<button type="button" class="btn btn-info me-1 mb-1" value="${row.id_mobil}" onclick="EditData(this)"><i class="fa fa-edit opacity-50 me-1"></i>Edit</button>`
                action += `<button type="button" class="btn btn-danger me-1 mb-1" value="${row.id_mobil}" onclick="DeleteData(this)"><i class="fa fa-trash opacity-50 me-1"></i>Delete</button>`
                action += `</div>`

                return action;
            }
        }
    ]
});

function test() {
    $.ajax({
        url: "http://localhost:5205/Ordering/getOrdering",
        type: "GET",
        cache: false,
        success: function (result) {
            console.log(result);


        }
    });
}

$("#btnAddData").on("click", function () {
    $("#txtNamaMobil").val("");
    $("#txtWarna").val("");
    $("#txtNoPlat").val("");
    $("#txtHarga").val("");
    $("#btnUpdate").hide();
    $("#btnSave").show();

});

$("#btnSave").on("click", function () {
    var obj = {
        nama_mobil: $("#txtNamaMobil").val(),
        warna: $("#txtWarna").val(),
        no_plat: $("#txtNoPlat").val(),
        harga: $("#txtHarga").val()
    }
    console.log(obj);
    $.ajax({
        url: "http://localhost:5047/api/Car/CreateCar",
        data: JSON.stringify(obj),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
            alert(data.remark);
            table.ajax.reload();
            $("#modal-default").modal('hide');
        },
        error: function (xhr) {
            alert(xhr.Message);
        }
    });
});

function EditData(data) {
    $("#modal-default").modal("show");
    $("#btnUpdate").show();
    $("#btnSave").hide();

    $.ajax({
        url: "http://localhost:5205/Car/getCarByID/" + data.value,
        type: "GET",
        cache: false,
        success: function (result) {
            console.log(result.data[0].nama_mobil);
            $("#txtNamaMobil").val(result.data[0].nama_mobil);
            $("#txtWarna").val(result.data[0].warna);
            $("#txtNoPlat").val(result.data[0].no_plat);
            $("#txtHarga").val(result.data[0].harga);
            $("#hdIdMobil").val(result.data[0].id_mobil);
        }
    });
}

$("#btnUpdate").on("click", function () {
    var obj = {
        id_mobil: $("#hdIdMobil").val(),
        nama_mobil: $("#txtNamaMobil").val(),
        warna: $("#txtWarna").val(),
        no_plat: $("#txtNoPlat").val(),
        harga: $("#txtHarga").val()
    }
    console.log(obj);
    $.ajax({
        url: "http://localhost:5047/api/Car/UpdateCar",
        data: JSON.stringify(obj),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
            alert(data.remark);
            table.ajax.reload();
            $("#modal-default").modal('hide');
        },
        error: function (xhr) {
            alert(xhr.Message);
        }
    });
});

function DeleteData(data) {
    var userConfirmed = confirm("Apakah Yakin Ingin Menghapus Data?");

    if (userConfirmed) {
        $.ajax({
            url: "http://localhost:5047/api/Car/DeleteCar/" + data.value,
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log(data);
                alert(data.remark);
                table.ajax.reload();
            },
            error: function (xhr) {
                alert(xhr.Message);
            }
        });
    } 

}
