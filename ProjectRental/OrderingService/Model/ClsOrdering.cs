﻿namespace OrderingService.Model
{
    public class ClsOrdering
    {
        public int id_pemesanan { get; set; }
        public int id_mobil { get; set; }
        public int id_pelanggan { get; set; }
        public string nama_mobil { get; set; }
        public string nama_pemesan { get; set; }
        public string tanggal_pemesanan { get; set; }
        public string tanggal_pengembalian { get; set; }
        public decimal total_harga { get; set; }
        public int total_hari { get; set; }
        public string status { get; set; }
    }

    public class ClsOrderingInput
    {
        public int id_mobil { get; set; }
        public int id_pelanggan { get; set; }
        public string tanggal_pemesanan { get; set; }
        public string tanggal_pengembalian { get; set; }
    }

    public class ClsOrderingUpdate
    {
        public int id_pemesanan { get; set; }
        public int id_mobil { get; set; }
        public int id_pelanggan { get; set; }
        public string tanggal_pemesanan { get; set; }
        public string tanggal_pengembalian { get; set; }
        public int status { get; set; }
    }
}
