﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using OrderingService.Model;
namespace OrderingService.Controllers
{
    public class OrderingController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public readonly IConfiguration _configuration;
        private readonly SqlConnection _connection;
        public OrderingController(IConfiguration configuration)
        {
            _configuration = configuration;
            string connectionString = _configuration.GetConnectionString("Db_Conn");
            _connection = new SqlConnection(connectionString);
            _connection.Open(); // Open the connection once
        }

        // Ensure the connection is closed when the controller is disposed
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _connection.Close();
                _connection.Dispose();
            }
            base.Dispose(disposing);
        }

        [Route("api/Ordering/getOrdering")]
        [HttpGet]
        public ActionResult<IEnumerable<ClsOrdering>> getOrdering()
        {
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM [DB_TEST_MANDIRI].[dbo].[VW_PEMESANAN]", _connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<ClsOrdering> OrderingList = new List<ClsOrdering>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ClsOrdering input = new ClsOrdering();
                    input.id_pemesanan = Convert.ToInt32(dt.Rows[i]["ID_PEMESANAN"]);
                    input.id_mobil = Convert.ToInt32(dt.Rows[i]["ID_MOBIL"]);
                    input.id_pelanggan = Convert.ToInt32(dt.Rows[i]["ID_PELANGGAN"]);
                    input.nama_mobil = Convert.ToString(dt.Rows[i]["NAMA_MOBIL"]);
                    input.nama_pemesan = Convert.ToString(dt.Rows[i]["NAMA_PEMESAN"]);
                    input.tanggal_pemesanan = Convert.ToString(dt.Rows[i]["TANGGAL_PEMESANAN"]);
                    input.tanggal_pengembalian = Convert.ToString(dt.Rows[i]["TANGGAL_PENGEMBALIAN"]);
                    input.total_harga = Convert.ToDecimal(dt.Rows[i]["TOTAL_HARGA"]);
                    input.total_hari = Convert.ToInt32(dt.Rows[i]["TOTAL_HARI"]);
                    input.status = Convert.ToString(dt.Rows[i]["STATUS"]);
                    OrderingList.Add(input);
                }
            }

            if (OrderingList.Count > 0)
            {
                return StatusCode(StatusCodes.Status200OK, new { data = OrderingList });
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound, new { data = "Data Not Found" });

            }
        }

        [Route("api/Ordering/getOrderingByID/{id}")]
        [HttpGet]
        public ActionResult<IEnumerable<ClsOrdering>> getOrderingByID(int id)
        {
            SqlDataAdapter da = new SqlDataAdapter($"SELECT * FROM [DB_TEST_MANDIRI].[dbo].[VW_PEMESANAN] WHERE ID_PEMESANAN = {id}", _connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<ClsOrdering> OrderingList = new List<ClsOrdering>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ClsOrdering input = new ClsOrdering();
                    input.id_pemesanan = Convert.ToInt32(dt.Rows[i]["ID_PEMESANAN"]);
                    input.id_mobil = Convert.ToInt32(dt.Rows[i]["ID_MOBIL"]);
                    input.id_pelanggan = Convert.ToInt32(dt.Rows[i]["ID_PELANGGAN"]);
                    input.nama_mobil = Convert.ToString(dt.Rows[i]["NAMA_MOBIL"]);
                    input.nama_pemesan = Convert.ToString(dt.Rows[i]["NAMA_PEMESAN"]);
                    input.tanggal_pemesanan = Convert.ToString(dt.Rows[i]["TANGGAL_PEMESANAN"]);
                    input.tanggal_pengembalian = Convert.ToString(dt.Rows[i]["TANGGAL_PENGEMBALIAN"]);
                    input.total_harga = Convert.ToDecimal(dt.Rows[i]["TOTAL_HARGA"]);
                    input.total_hari = Convert.ToInt32(dt.Rows[i]["TOTAL_HARI"]);
                    input.status = Convert.ToString(dt.Rows[i]["STATUS"]);
                    OrderingList.Add(input);
                }
            }

            if (OrderingList.Count > 0)
            {
                return StatusCode(StatusCodes.Status200OK, new { data = OrderingList });
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { data = "Data Not Found" });

            }
        }

        [Route("api/Ordering/CreateOrdering")]
        [HttpPost]
        public IActionResult CreateOrdering([FromBody] ClsOrderingInput param)
        {
            try
            {
                SqlCommand da = new SqlCommand($"exec dbo.sp_insert_pemesananan {param.id_mobil},{param.id_pelanggan},'{param.tanggal_pemesanan}','{param.tanggal_pengembalian}'", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Disimpan !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Insert Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }

        [Route("api/Ordering/UpdateOrdering")]
        [HttpPost]
        public IActionResult UpdateOrdering([FromBody] ClsOrderingUpdate param)
        {
            try
            {
                SqlCommand da = new SqlCommand($"exec dbo.sp_update_pemesananan {param.id_pemesanan},{param.id_mobil},{param.id_pelanggan}, '{param.tanggal_pemesanan}','{param.tanggal_pengembalian}',{param.status}", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Diupdate !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Update Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }

        [Route("api/Ordering/DeleteOrdering/{id}")]
        [HttpPost]
        public IActionResult DeleteOrdering(int id)
        {
            try
            {
                SqlCommand da = new SqlCommand($"DELETE FROM [dbo].[TBL_T_PEMESANAN] WHERE ID_PEMESANAN = {id}", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Dihapus !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Hapus Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }
    }
}
