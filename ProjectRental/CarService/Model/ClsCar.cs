﻿namespace CarService.Model
{
    public class ClsCar
    {
        public int id_mobil { get; set; }
        public string nama_mobil { get; set; }
        public string warna { get; set; }
        public string no_plat { get; set; }
        public decimal harga { get; set; }
    }

    public class ClsCarInput
    {
        public string nama_mobil { get; set; }
        public string warna { get; set; }
        public string no_plat { get; set; }
        public decimal harga { get; set; }
    }
}
