﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CarService.Model;

namespace CarService.Controllers
{
    public class CarController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public readonly IConfiguration _configuration;
        private readonly SqlConnection _connection;
        public CarController(IConfiguration configuration)
        {
            _configuration = configuration;
            string connectionString = _configuration.GetConnectionString("Db_Conn");
            _connection = new SqlConnection(connectionString);
            _connection.Open(); // Open the connection once
        }

        // Ensure the connection is closed when the controller is disposed
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _connection.Close();
                _connection.Dispose();
            }
            base.Dispose(disposing);
        }

        [Route("api/Car/getCar")]
        [HttpGet]
        public ActionResult<IEnumerable<ClsCar>> getCar()
        {
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT ID_MOBIL, NAMA_MOBIL, WARNA, NO_PLAT, HARGA FROM [DB_TEST_MANDIRI].[dbo].[TBL_M_MOBIL]", _connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<ClsCar> carList = new List<ClsCar>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ClsCar input = new ClsCar();
                    input.id_mobil = Convert.ToInt32(dt.Rows[i]["ID_MOBIL"]);
                    input.nama_mobil = Convert.ToString(dt.Rows[i]["NAMA_MOBIL"]);
                    input.warna = Convert.ToString(dt.Rows[i]["WARNA"]);
                    input.no_plat = Convert.ToString(dt.Rows[i]["NO_PLAT"]);
                    input.harga = Convert.ToDecimal(dt.Rows[i]["HARGA"]);
                    carList.Add(input);
                }
            }

            if (carList.Count > 0)
            {
                return StatusCode(StatusCodes.Status200OK, new { data = carList });
            }
            else
            {
                return StatusCode(StatusCodes.Status404NotFound, new { data = "Data Not Found" });

            }
        }

        [Route("api/Car/getCarByID/{id}")]
        [HttpGet]
        public ActionResult<IEnumerable<ClsCar>> getCarByID(int id)
        {
            SqlDataAdapter da = new SqlDataAdapter($"SELECT ID_MOBIL, NAMA_MOBIL, WARNA, NO_PLAT, HARGA FROM [DB_TEST_MANDIRI].[dbo].[TBL_M_MOBIL] WHERE ID_MOBIL = {id}", _connection);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<ClsCar> carList = new List<ClsCar>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ClsCar input = new ClsCar();
                    input.id_mobil = Convert.ToInt32(dt.Rows[i]["ID_MOBIL"]);
                    input.nama_mobil = Convert.ToString(dt.Rows[i]["NAMA_MOBIL"]);
                    input.warna = Convert.ToString(dt.Rows[i]["WARNA"]);
                    input.no_plat = Convert.ToString(dt.Rows[i]["NO_PLAT"]);
                    input.harga = Convert.ToDecimal(dt.Rows[i]["HARGA"]);
                    carList.Add(input);
                }
            }

            if (carList.Count > 0)
            {
                return StatusCode(StatusCodes.Status200OK, new { data = carList });
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { data = "Data Not Found" });

            }
        }

        [Route("api/Car/CreateCar")]
        [HttpPost]
        public IActionResult CreateCar([FromBody] ClsCarInput param)
        {
            try
            {
                SqlCommand da = new SqlCommand($"INSERT INTO [dbo].[TBL_M_MOBIL] (NAMA_MOBIL, WARNA, NO_PLAT, HARGA) VALUES ('{param.nama_mobil}','{param.warna}','{param.no_plat}','{param.harga}')", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Disimpan !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Insert Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }

        [Route("api/Car/UpdateCar")]
        [HttpPost]
        public IActionResult UpdateCar([FromBody] ClsCar param)
        {
            try
            {
                SqlCommand da = new SqlCommand($"UPDATE [dbo].[TBL_M_MOBIL] SET NAMA_MOBIL = '{param.nama_mobil}', WARNA = '{param.warna}', NO_PLAT = '{param.no_plat}', HARGA = {param.harga} WHERE ID_MOBIL = {param.id_mobil}", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Diupdate !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Update Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }

        [Route("api/Car/DeleteCar/{id}")]
        [HttpPost]
        public IActionResult DeleteCar(int id)
        {
            try
            {
                SqlCommand da = new SqlCommand($"DELETE FROM [dbo].[TBL_M_MOBIL] WHERE ID_MOBIL = {id}", _connection);
                int rowAffected = da.ExecuteNonQuery();

                if (rowAffected > 0)
                {
                    return StatusCode(StatusCodes.Status200OK, new { remark = "Data Berhasil Dihapus !" });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Gagal Hapus Data !" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { remark = "Error : " + ex.Message });
            }
        }
    }
}
